import NumberFormat from 'react-number-format'
import { Table } from 'antd'

const columns = [
  {
    title: 'Code Number',
    dataIndex: 'codeNumber',
    key: 'codeNumber',
    align: 'right' as const,
    defaultSortOrder: 'ascend' as const,
    sorter: {
      compare: (a, b) => a.codeNumber - b.codeNumber,
    },
  },
  {
    title: 'Origin Country',
    dataIndex: 'originCountry',
    key: 'originCountry',
    sorter: {
      compare: (a, b) => a.originCountry.localeCompare(b.originCountry),
    },
  },
  {
    title: 'Country Code',
    dataIndex: 'orig',
    key: 'orig',
  },
  {
    title: 'Delivery',
    dataIndex: 'delivery',
    key: 'delivery',
    align: 'right' as const,
  },
  {
    title: 'Delivery Date',
    dataIndex: 'deliveryDate',
    key: 'deliveryDate',
    align: 'center' as const,
  },
  {
    title: 'Bill. Doc.',
    dataIndex: 'billDoc',
    key: 'billDoc',
    align: 'right' as const,
  },
  {
    title: 'Billing Date',
    dataIndex: 'billingDate',
    align: 'center' as const,
    key: 'billingDate',
  },
  {
    title: 'Material Number',
    dataIndex: 'materialNumber',
    key: 'materialNumber',
    align: 'right' as const,
  },
  {
    title: 'Billed Quantity',
    dataIndex: 'billedQuantity',
    key: 'billedQuantity',
    align: 'right' as const,
  },
  {
    title: 'SU',
    dataIndex: 'su',
    key: 'su',
  },
  {
    title: 'Net Weight',
    dataIndex: 'netWeight',
    key: 'netWeight',
    align: 'right' as const,
    render: (netWeight) => `${netWeight} kg`,
  },
  {
    title: 'Gross Weight',
    dataIndex: 'grossWeight',
    key: 'grossWeight',
    align: 'right' as const,
  },
  {
    title: 'Net Value / EUR',
    dataIndex: 'netValue',
    key: 'netValue',
    align: 'right' as const,
  },
  {
    title: 'Price/HUF',
    dataIndex: 'pricePerHuf',
    key: 'pricePerHuf',
    render: (pricePerHuf) => (
      <NumberFormat
        value={parseFloat(pricePerHuf).toFixed(2)}
        displayType={'text'}
        thousandSeparator={true}
      />
    ),
  },
  {
    title: 'Value/HUF',
    dataIndex: 'valuePerHuf',
    key: 'valuePerHuf',
    render: (valuePerHuf) => (
      <NumberFormat
        value={parseFloat(valuePerHuf).toFixed()}
        displayType={'text'}
        thousandSeparator={' '}
      />
    ),
  },
]

interface DataTableProps {
  dataSource: any
}

const MainDatatable = (props: DataTableProps) => {
  const { dataSource } = props

  return (
    <>
      {dataSource.length !== 0 && (
        <Table
          columns={columns}
          dataSource={dataSource}
          pagination={{
            defaultPageSize: 25,
            pageSizeOptions: ['25', '50', '100', dataSource.length],
          }}
        />
      )}
    </>
  )
}

export default MainDatatable
