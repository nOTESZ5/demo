import NumberFormat from 'react-number-format'
import { Table, Typography } from 'antd'
import '../css/summaryDatatable.scss'

const columns = [
  {
    title: 'ID',
    dataIndex: 'key',
    key: 'key',
    defaultSortOrder: 'ascend' as const,
  },
  {
    title: 'Code Number',
    dataIndex: 'codeNumber',
    key: 'codeNumber',
    align: 'center' as const,
    sorter: {
      compare: (a, b) => a.codeNumber - b.codeNumber,
    },
  },
  {
    title: 'Country Code',
    dataIndex: 'orig',
    key: 'orig',
    align: 'center' as const,
  },
  {
    title: 'Origin Country',
    dataIndex: 'originCountry',
    key: 'originCountry',
    align: 'center' as const,
    sorter: {
      compare: (a, b) => a.originCountry.localeCompare(b.originCountry),
    },
  },
  {
    title: 'Net Weight',
    dataIndex: 'netWeight',
    key: 'netWeight',
    align: 'center' as const,
    render: (netWeight) => `${parseFloat(netWeight).toFixed(2)} kg`,
  },
  {
    title: 'Billed Quantity',
    dataIndex: 'billedQuantity',
    key: 'billedQuantity',
    align: 'center' as const,
    render: (valuePerHuf) => (
      <NumberFormat
        value={parseFloat(valuePerHuf).toFixed(0)}
        displayType={'text'}
        thousandSeparator={' '}
      />
    ),
  },

  {
    title: 'Value/HUF',
    dataIndex: 'valuePerHuf',
    key: 'valuePerHuf',
    align: 'center' as const,
    render: (valuePerHuf) => (
      <NumberFormat
        value={parseFloat(valuePerHuf).toFixed(0)}
        displayType={'text'}
        thousandSeparator={' '}
      />
    ),
  },
]

interface DataTableProps {
  dataSource: any
  sumData: {
    netWeightSum: number
    quantitySum: number
    valueInHufSum: number
  }
}

const footer = ({ netWeightSum, quantitySum, valueInHufSum }) => {
  const { Text } = Typography

  return (
    <div className="footer">
      <Text strong>Summary:</Text>
      <span className="netWeightSum">{`${parseFloat(netWeightSum).toFixed(
        2
      )} kg`}</span>
      {
        <NumberFormat
          value={parseFloat(quantitySum).toFixed(0)}
          displayType={'text'}
          thousandSeparator={' '}
          className="quantitySum"
        />
      }
      {
        <NumberFormat
          value={parseFloat(valueInHufSum).toFixed(0)}
          displayType={'text'}
          thousandSeparator={' '}
          suffix={' HUF'}
          className="valueInHufSum"
        />
      }
    </div>
  )
}

const SummaryDataTable = (props: DataTableProps) => {
  const { dataSource, sumData } = props
  return (
    <>
      {dataSource.length !== 0 && (
        <Table
          columns={columns}
          dataSource={dataSource}
          pagination={{
            defaultPageSize: 25,
            pageSizeOptions: ['25', '50', '100', dataSource.length],
          }}
          footer={() => footer(sumData)}
        />
      )}
    </>
  )
}

export default SummaryDataTable
