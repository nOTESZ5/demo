import { Upload, Button, message, Spin } from 'antd'
import { UploadOutlined } from '@ant-design/icons'
import dayjs from 'dayjs'
import axios from 'axios'
import readXlsxFile from 'read-excel-file'

interface UploadButtonProps {
  setXlsxData: (data) => void
  setSummaryData: (data: any[]) => void
  setLoading: (loading: boolean) => void
  loading: boolean
  className: string
  setSumData: (data: {
    netWeightSum: number
    quantitySum: number
    valueInHufSum: number
  }) => void
}
const UploadButton = (props: UploadButtonProps) => {
  const {
    setXlsxData,
    className,
    setLoading,
    loading,
    setSummaryData,
    setSumData,
  } = props

  const fetchCurrenciesAndMapData = async (xlsxData) => {
    const data: any[] = []

    setLoading(true)

    let index: number = 0
    let netWeightSum: number = 0
    let quantitySum: number = 0
    let valueInHufSum: number = 0

    for (const row of xlsxData) {
      if (index > 0) {
        await axios
          .get(
            `https://api.exchangerate.host/${dayjs(
              new Date(row[12]).toISOString()
            ).format('YYYY-M-D')}`
          )
          // eslint-disable-next-line no-loop-func
          .then((res) => {
            const hufPrice = parseFloat(
              parseFloat(res.data.rates.HUF).toFixed(2)
            )

            data.push({
              key: index.toString(),
              delivery: row[0],
              deliveryDate: dayjs(new Date(row[11]).toISOString()).format(
                'YYYY-M-D'
              ),
              billDoc: row[4],
              billingDate: dayjs(new Date(row[12]).toISOString()).format(
                'YYYY-M-D'
              ),
              materialNumber: row[5],
              billedQuantity: row[13],
              su: row[6],
              netWeight: row[15],
              grossWeight: row[17],
              netValue: row[19],
              codeNumber: row[8],
              originCountry: row[10],
              orig: row[9],
              pricePerHuf: hufPrice,
              valuePerHuf: hufPrice * row[19],
            })

            netWeightSum += row[15]
            quantitySum += row[13]
            valueInHufSum += hufPrice * row[19]

            setSumData({ netWeightSum, quantitySum, valueInHufSum })
          })
          .catch((err) => console.log(err))
      }
      index++
    }

    const summaryData = [
      ...data
        .reduce((r, o) => {
          const key = o.codeNumber + '-' + o.orig

          const item =
            r.get(key) ||
            Object.assign({}, o, {
              billedQuantity: 0,
              netWeight: 0,
              valuePerHuf: 0,
            })

          item.billedQuantity += o.billedQuantity
          item.netWeight += o.netWeight
          item.valuePerHuf += o.valuePerHuf

          return r.set(key, item)
        }, new Map())
        .values(),
    ].map((value, index) => {
      return { ...value, key: index + 1 }
    })

    setSummaryData(summaryData)

    return data
  }

  const uploadProps = {
    name: 'file',
    multiple: false,
    accept: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    onChange(info: any) {
      if (
        info.file.type !==
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
      ) {
        message.error('Not an XLSX!')
      } else {
        readXlsxFile(info.file.originFileObj)
          .then((rows: any) => {
            const acceptedColumnHeaders = [
              'Delivery',
              'Sold-to pt',
              'Ship-to',
              'Payer',
              'Bill.Doc.',
              'Material',
              'SU',
              'BUn',
              'Code number',
              'Orig',
              'Sh.name',
              'Deliv.Date',
              'Billing Date',
              'Billed Quantity',
              'SU',
              'Net weight',
              'WUn',
              'Gross weight',
              'WUn',
              'Net value',
              'Curr.',
            ]

            if (
              JSON.stringify(rows[0]) !== JSON.stringify(acceptedColumnHeaders)
            ) {
              message.error('Please choose an XLSX with an appropriate format.')
            }

            if (rows.length <= 1) {
              message.error('The file is empty.')
            }
            return fetchCurrenciesAndMapData(rows)
          })
          .then((data) => {
            setLoading(false)
            setXlsxData(data)
          })
          .catch((err) => console.log(err))
      }
    },
    customRequest() {},
    showUploadList: false,
  }

  return loading ? (
    <Spin />
  ) : (
    <Upload {...uploadProps} className={className}>
      <Button icon={<UploadOutlined />}>Upload XLSX</Button>
    </Upload>
  )
}

export default UploadButton
