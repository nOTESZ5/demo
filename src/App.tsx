import { useState } from 'react'
import { Tabs } from 'antd'
import MainDatatable from './components/MainDataTable'
import UploadButton from './components/Upload'
import 'antd/dist/antd.css'
import './css/app.scss'
import SummaryDataTable from './components/SummaryDataTable'

const { TabPane } = Tabs

const App = () => {
  const [xlsxData, setXlsxData] = useState([])
  const [summaryData, setSummaryData] = useState<any[]>([])
  const [loading, setLoading] = useState(false)
  const [sumData, setSumData] = useState({
    netWeightSum: 0,
    quantitySum: 0,
    valueInHufSum: 0,
  })

  return (
    <>
      {xlsxData.length === 0 ? (
        <div className="welcome">
          <UploadButton
            setXlsxData={setXlsxData}
            setSummaryData={setSummaryData}
            className="uploadButton"
            loading={loading}
            setLoading={setLoading}
            setSumData={setSumData}
          />
        </div>
      ) : (
        <div className="datatables">
          <Tabs defaultActiveKey="1">
            <TabPane tab="XLSX Data" key="1">
              <MainDatatable dataSource={xlsxData} />
            </TabPane>
            <TabPane tab="Summary" key="2">
              <SummaryDataTable dataSource={summaryData} sumData={sumData} />
            </TabPane>
          </Tabs>
        </div>
      )}
    </>
  )
}

export default App
